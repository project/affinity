
  author : Kevin Mathis (sicjoy)
  contact: kevin [at] vervemojo [dot] com

  The Affinity module is intended to be used as a catalyst for community
  interaction on sites where users rate the content.  It works in conjunction
  with the Voting API module to match users to other users with similar voting
  patterns. The module provides a block showing users you may have an affinity
  for.

  SETTINGS

  Affinity recalculation cycle - There is a trade-off between users having up
  to date Affinity Matches and server load.  This setting addresses this issue.
  The affinity caclulation results for each user are cached in a database
  table.  Affinity matches don't get recalculated until the cache's timestamp
  is older than the set period.

  Number of matches - This is the number of matches to calculate for each user.

  Common vote threshold - Sets the minimum number of votes in common required
  before two users can be considered in the matching algorithm.  Setting this
  higher will result in shorter calculation times.  However, it will require
  users to cast at least this many votes before they can begin receiving
  matches.

  Votingapi content type - This will depend on your site's voting system.  Most
  likely it should be set to either node or comment.

  Votingapi value type - Depends on your site's voting system.  Typical values
  include percent or points.

  Votingapi tag - Again, this depends on you site's voting system.  In most
  cases use vote, but it can be anything.  The External Link Popularity module
  uses pop_links for this value.

  MATCHES BLOCK SETTINGS

  Block title - Here you can give the block a different title.  The default value
  is Affinity Matches.

  Empty block text - This is the text shown in the block when a user doesn't have
  any matches yet.  This will happen, for example, when a user hasn't cast any
  votes.

  Anonymous user text - This is the text shown in the block for anonymous users.

  Show user pictures - Check this box if you want to show user picures in the
  affinity block.  Make sure that user pictures are enabled at admin/user/settings.
  You should also assign a default picture for the case where users choose not to
  upload a picture.

  Enjoy,

  sicjoy
